1. Config vim:
```bash
# Install vim
$ sudo apt-get install -y vim
# Clone this repo
$ git clone git@bitbucket.org:tienhm_/dotvim.git ~/dotvim/
# Copy .vimrc
$ cp ~/dotvim/.vimrc ~/.vimrc
# Install vim Bundle
$ git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
# Install vim plugin
$ vim +PluginInstall +qall
```

2. Config fish:
```bash
# Install fish base on your os version
# Config
$ cp ~/dotvim/config.fish ~/.config/fish/config.fish
$ cp ~/dotvim/fish_prompt.fish ~/.config/fish/functions/fish_prompt.fish
# Change fish config
$ fish_config
# Select git informative
```
