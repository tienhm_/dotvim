#!/bin/bash

set -x

if [ $1 == 'linux' ]; then
    echo "Running in Linux ..."
    apt-get update -y
    sed 's/#.*//' requirements.txt | xargs apt-get install -y
    sed 's/#.*//' requirements-cask.txt | xargs apt-get install -y
else
    echo "Running in MacOS ..."
    sed 's/#.*//' requirements.txt | xargs brew install
    sed 's/#.*//' requirements-cask.txt | xargs brew install --cask
fi

# Install vim
cp .vimrc ~/.vimrc
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall

# Install fish
cp config.fish ~/.config/fish/config.fish
cp fish_prompt.fish ~/.config/fish/functions/fish_prompt.fish